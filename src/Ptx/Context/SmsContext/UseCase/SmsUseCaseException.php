<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\UseCase;

class SmsUseCaseException extends \Exception
{
}
