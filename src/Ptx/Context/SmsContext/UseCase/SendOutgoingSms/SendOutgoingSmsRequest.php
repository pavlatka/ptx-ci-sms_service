<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\UseCase\SendOutgoingSms;

class SendOutgoingSmsRequest
{
    private $sender;
    private $recipient;

    public function __construct(int $sender, string $recipient)
    {
        $this->sender    = $sender;
        $this->recipient = $recipient;
    }

    public function getSender() : int
    {
        return $this->sender;
    }

    public function getRecipient() : string
    {
        return $this->recipient;
    }
}
