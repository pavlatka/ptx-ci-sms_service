<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\UseCase\SendOutgoingSms;

use Ptx\Context\SmsContext\Entity\OutgoingSms;
use Ptx\Context\SmsContext\Entity\DeliveryReport;

class SendOutgoingSmsResponse
{
    private $outgoingSms;
    private $deliveryReport;

    public function __construct(
        OutgoingSms $outgoingSms,
        DeliveryReport $deliveryReport
    )
    {
        $this->outgoingSms     = $outgoingSms;
        $this->deliveryReport = $deliveryReport;
    }

    public function getId() : int
    {
        return $this->outgoingSms->getId();
    }

    public function getSender() : int
    {
        return $this->outgoingSms->getSender();
    }

    public function getRecipient() : string
    {
        return $this->outgoingSms->getRecipient();
    }

    public function getContent() : string
    {
        return $this->outgoingSms->getContent();
    }

    public function getDeliveryStatus() : int
    {
        return $this->deliveryReport->getStatus();
    }
}
