<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\UseCase\SendOutgoingSms;

use Ptx\Context\SmsContext\Entity\ShortCode;
use Ptx\Context\SmsContext\Entity\DeliveryReport;
use Ptx\Context\SmsContext\Repository\OutgoingSmsRepository;
use Ptx\Context\SmsContext\Repository\DeliveryReportRepository;

class SendOutgoingSmsUseCase
{
    private $smsRepository;
    private $reportRepository;
    private $shortCodeRepository;

    public function __construct(
        OutgoingSmsRepository $smsRepository,
        DeliveryReportRepository $reportRepository,
        ShortCodeRepository $shortCodeRepository
    ) {
        $this->smsRepository       = $smsRepository;
        $this->reportRepository    = $reportRepository;
        $this->shortCodeRepository = $shortCodeRepository;
    }

    public function sendOutgoingSms(SendOngoingSmsRequest $request) : SendOngoingSmsResponse
    {
        try {
            $sender    = $request->getSender();
            $recipient = $request->getRecipient();
            $shortCode = $this->shortCodeRepository->getShortCode4Code($sender);

            $outgoingSms = $this->smsRepository->createSms($shortCode->getId(), $recipient, $shortCode->getConfirmationText());
            $this->smsRepository->persist($smsSms);

            $deliveryReport = $this->reportRepository->createReport4Sms($outgoingSms, DeliveryReport::STATUS_ACCEPTED);
            $this->reportRepository->perist($deliveryReport);

            return new SendOngoingSmsResponse($outgoingSms, $deliveryReport);
        } catch (\Exception $e) {
            throw new SmsUseCaseException(
                'An error occurred while processing your request. [I:' . $e->getMessage() .']',
                SmsUseCaseException::ERROR_RUNTIME_ERROR
            );
        }
    }
}
