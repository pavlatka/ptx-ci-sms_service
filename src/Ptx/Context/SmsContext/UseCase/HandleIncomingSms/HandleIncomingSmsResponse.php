<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\UseCase\HandleIncomingSms;

use Ptx\Context\SmsContext\Entity\IncomingSms;
use Ptx\Context\SmsContext\Entity\DeliveryReport;

class HandleIncomingSmsResponse
{
    private $incomingSms;
    private $deliveryReport;

    public function __construct(
        IncomingSms $incomingSms,
        DeliveryReport $deliveryReport
    )
    {
        $this->incomingSms     = $incomingSms;
        $this->deliveryReport = $deliveryReport;
    }

    public function getId() : int
    {
        return $this->incomingSms->getId();
    }

    public function getSender() : string
    {
        return $this->incomingSms->getSender();
    }

    public function getRecipient() : int
    {
        return $this->incomingSms->getRecipient();
    }

    public function getContent() : string
    {
        return $this->incomingSms->getContent();
    }

    public function getDeliveryStatus() : int
    {
        return $this->deliveryReport->getStatus();
    }
}
