<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\UseCase\HandleIncomingSms;

use Ptx\Context\SmsContext\Entity\BaseSms;
use Ptx\Context\SmsContext\Entity\IncomingSms;
use Ptx\Context\SmsContext\Entity\OutgoingSms;
use Ptx\Context\SmsContext\Entity\DeliveryReport;
use Ptx\Context\SmsContext\Repository\IncomingSmsRepository;
use Ptx\Context\SmsContext\Repository\OutgoingSmsRepository;
use Ptx\Context\SmsContext\Repository\DeliveryReportRepository;
use Ptx\Context\SmsContext\Repository\ShortCodeRepository;

class HandleIncomingSmsUseCase
{
    private $reportRepository;
    private $shortCodeRepository;
    private $incomingSmsReporitory;
    private $outgoingSmsReporitory;

    public function __construct(
        IncomingSmsRepository $incomingSmsReporitory,
        OutgoingSmsRepository $outgoingSmsReporitory,
        DeliveryReportRepository $reportRepository,
        ShortCodeRepository $shortCodeRepository,
    ) {
        $this->reportRepository      = $reportRepository;
        $this->shortCodeRepository   = $shortCodeRepository;
        $this->incomingSmsReporitory = $incomingSmsReporitory;
        $this->outgoingSmsReporitory = $outgoingSmsReporitory;
    }

    public function sendIncomingSms(SendOngoingSmsRequest $request) : SendOngoingSmsResponse
    {
        try {
            $sender    = $request->getSender();
            $content   = $request->getContent();
            $shortCode = $this->shortCodeRepository->getShortCode4Code($request->getRecipient());

            $incomingSms    = $this->saveIncomingSms($shortCode, $sender, $content);
            $deliveryReport = $this->saveDeliveryReport($incomingSms);

            $outgoingSms    = $this->saveOutgoingSms($shortCode, $sender);
            $outgoingReport = $this->saveDeliveryReport($outgoingSms);

            return new SendOngoingSmsResponse($incomingSms, $deliveryReport);
        } catch (\Exception $e) {
            throw new SmsUseCaseException(
                'An error occurred while processing your request. [I:' . $e->getMessage() .']',
                SmsUseCaseException::ERROR_RUNTIME_ERROR
            );
        }
    }

    protected function saveIncomingSms(ShortCode $shortCode, string $sender, string $content) : IncomingSms
    {
        $incomingSms = $this->smsRepository->createSms($shortCode->getId(), $sender, $content);
        $this->smsRepository->persist($incomingSms);

        return $incomingSms;
    }

    protected function saveDeliveryReport(BaseSms $sms) : DeliveryReport
    {
        $deliveryReport = $this->reportRepository->createReport4Sms($sms, DeliveryReport::STATUS_ACCEPTED);
        $this->reportRepository->perist($deliveryReport);

        return $deliveryReport;
    }

    public function saveOutgoingSms(ShortCode $shortCode, string $sender) : OutgoingSms
    {
        $outgoingSms = $this->smsRepository->createSms($shortCode->getId(), $sender, $content->getConfirmationText());
        $this->smsRepository->persist($outgoingSms);

        return $outgoingSms;
    }
}
