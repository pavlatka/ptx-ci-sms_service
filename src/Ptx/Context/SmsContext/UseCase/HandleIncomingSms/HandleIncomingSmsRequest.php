<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\UseCase\HandleIncomingSms;

class HandleIncomingSmsRequest
{
    private $sender;
    private $content;
    private $recipient;

    public function __construct(string $sender, int $recipient, string $content)
    {
        $this->sender    = $sender;
        $this->content   = $content;
        $this->recipient = $recipient;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function getRecipient() : int
    {
        return $this->recipient;
    }

    public function getSender() : string
    {
        return $this->sender;
    }
}
