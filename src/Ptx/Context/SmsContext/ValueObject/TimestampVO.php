<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\ValueObject;

class TimestampVO
{
    private $timestamp;

    public function __construct(int $timestamp)
    {
        $this->timestamp = $timestamp;
    }

    public function getTimestamp() : int
    {
        return $this->timestamp;
    }
}
