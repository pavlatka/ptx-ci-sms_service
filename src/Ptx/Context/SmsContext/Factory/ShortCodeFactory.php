<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Factory;

use Ptx\Context\SmsContext\Entity\ShortCode;

class ShortCodeFactory extends BaseFactory
{
    public function createEntity(array $data) : ShortCode
    {
        $data += array(
            'id'                => null,
            'confirmation_text' => null
        );

        $entity = new ShortCode();
        $this->mapData2Entity($data, $entity);

        return $entity;
    }
}
