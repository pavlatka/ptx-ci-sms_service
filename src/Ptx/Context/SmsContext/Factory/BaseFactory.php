<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Factory;

use Ptx\Service\StringModifier;
use Ptx\Context\SmsContext\Entity\BaseEntity;

abstract class BaseFactory
{
    private $stringModifier;

    public function __construct(StringModifier $stringModifier)
    {
        $this->stringModifier = $stringModifier;
    }

    protected function mapData2Entity(array $array, BaseEntity $entity) : void
    {
        foreach ($array as $key => $value) {
            if ($value !== null) {
                $setFunction = 'set' . $this->stringModifier->camelize($key);
                $entity->$setFunction($value);
            }
        }
    }
}
