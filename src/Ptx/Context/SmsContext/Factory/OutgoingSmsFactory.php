<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Factory;

use Ptx\Context\SmsContext\Entity\OutgoingSms;

class OutgoingSmsFactory extends BaseFactory
{
    public function createEntity(array $data) : OutgoingSms
    {
        $data += array(
            'id'        => null,
            'recipient' => null,
            'sender'    => null,
            'content'   => null
        );

        $entity = new OutgoingSms();
        $this->mapData2Entity($data, $entity);

        return $entity;
    }
}
