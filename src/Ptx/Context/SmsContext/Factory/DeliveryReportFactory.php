<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Factory;

use Ptx\Context\SmsContext\Entity\DeliveryReport;
use Ptx\Context\SmsContext\ValueObject\TimestampVO;

class DeliveryReportFactory extends BaseFactory
{
    public function createEntity(array $data) : DeliveryReport
    {
        $data += array(
            'id'        => null,
            'status'    => null,
            'timestamp' => null
        );

        if ($data['timestamp'] !== null) {
            $data['timestamp'] = new TimestampVO($data['timestamp']);
        }

        $entity = new DeliveryReport();
        $this->mapData2Entity($data, $entity);

        return $entity;
    }
}
