<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Factory;

use Ptx\Context\SmsContext\Entity\IncomingSms;

class IncomingSmsFactory extends BaseFactory
{
    public function createEntity(array $data) : IncomingSms
    {
        $data += array(
            'id'        => null,
            'recipient' => null,
            'sender'    => null,
            'content'   => null
        );

        $entity = new IncomingSms();
        $this->mapData2Entity($data, $entity);

        return $entity;
    }
}
