<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Factory;

use Ptx\Context\SmsContext\Entity\Customer;

class CustomerFactory extends BaseFactory
{
    public function createEntity(array $data) : Customer
    {
        $data += array(
            'id'       => null,
            'sms_code' => null,
        );

        $entity = new Customer();
        $this->mapData2Entity($data, $entity);

        return $entity;
    }
}
