<?php declare(strict_types=1);

namespace Ptx/Context/SmsContext/Dao;

use Ptx/Database/Interfaces/QueryBuilderInterface;

abstract class BaseDao
{
    protected $databaes;

    public function __construct(QueryBuilderInterface $database)
    {
        $this->database = $database;
    }
}
