<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Dao;

use Ptx\Context\SmsContext\Entity\ShortCode;

class ShortCodeDao extends BaseDao
{
    public function getShortCode4Code(int $code) : array
    {
        // TODO: Implement query to find out data about the
        // short code accorging to its code
    }

    public function persist(ShortCode $sms)
    {
        // TODO: Save data into the database
    }
}
