<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Dao;

use Ptx\Context\SmsContext\Entity\DeliveryReport;

class DeliveryReportDao extends BaseDao
{
    public function persist(DeliveryReport $sms)
    {
        // TODO: Save data into the database
    }
}
