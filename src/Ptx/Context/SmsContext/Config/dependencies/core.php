<?php

$container['sms_short_code_dao'] = function ($c) {
    return new \Ptx\Context\SmsContext\Dao\ShortCodeDao(
        $c['db_query_builder']
    );
};

$container['sms_short_code_factory'] = function ($c) {
    return new \Ptx\Context\SmsContext\Factory\ShortCodeFactory(
        $c['service_string_modifire']
    );
};

$container['sms_short_code_repository'] = function ($c) {
    return new \Ptx\Context\SmsContext\Repository\ShortCodeRepository(
        $c['sms_short_code_dao'],
        $c['sms_short_code_factory']
    );
};

$container['sms_customer_dao'] = function ($c) {
    return new \Ptx\Context\SmsContext\Dao\CustomerDao(
        $c['db_query_builder']
    );
};

$container['sms_customer_factory'] = function ($c) {
    return new \Ptx\Context\SmsContext\Factory\CustomerFactory(
        $c['service_string_modifire']
    );
};

$container['sms_customer_repository'] = function ($c) {
    return new \Ptx\Context\SmsContext\Repository\CustomerRepository(
        $c['sms_customer_dao'],
        $c['sms_customer_factory']
    );
};

$container['sms_delivery_report_dao'] = function ($c) {
    return new \Ptx\Context\SmsContext\Dao\DeliveryReportDao(
        $c['db_query_builder']
    );
};

$container['sms_delivery_report_factory'] = function ($c) {
    return new \Ptx\Context\SmsContext\Factory\DeliveryReportFactory(
        $c['service_string_modifire']
    );
};

$container['sms_delivery_report_repository'] = function ($c) {
    return new \Ptx\Context\SmsContext\Repository\DeliveryReportRepository(
        $c['sms_delivery_report_dao'],
        $c['sms_delivery_report_factory']
    );
};

$container['sms_incoming_sms_dao'] = function ($c) {
    return new \Ptx\Context\SmsContext\Dao\IncomingSmsDao(
        $c['db_query_builder']
    );
};

$container['sms_incoming_sms_factory'] = function ($c) {
    return new \Ptx\Context\SmsContext\Factory\IncomingSmsFactory(
        $c['service_string_modifire']
    );
};

$container['sms_incoming_sms_repository'] = function ($c) {
    return new \Ptx\Context\SmsContext\Repository\IncomingSmsRepository(
        $c['sms_incoming_sms_dao'],
        $c['sms_incoming_sms_factory']
    );
};

$container['sms_outgoing_sms_dao'] = function ($c) {
    return new \Ptx\Context\SmsContext\Dao\OutgoingSmsDao(
        $c['db_query_builder']
    );
};

$container['sms_outgoing_sms_factory'] = function ($c) {
    return new \Ptx\Context\SmsContext\Factory\OutgoingSmsFactory(
        $c['service_string_modifire']
    );
};

$container['sms_outgoing_sms_repository'] = function ($c) {
    return new \Ptx\Context\SmsContext\Repository\OutgoingSmsRepository(
        $c['sms_outgoing_sms_dao'],
        $c['sms_outgoing_sms_factory']
    );
};
