<?php

$container['sms_uc_handle_incoming_sms'] = function ($c) {
    return new \Ptx\Context\SmsContext\UseCase\HandleIncomingSms\HandleIncomingSmsUseCase(
        $c['sms_incoming_sms_repository'],
        $c['sms_outgoing_sms_repository'],
        $c['sms_delivery_report_repository'],
        $c['sms_short_code_repository']
    );
};
