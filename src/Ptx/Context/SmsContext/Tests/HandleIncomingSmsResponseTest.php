<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Tests;

use Ptx\Context\SmsContext\Entity\IncomingSms;
use Ptx\Context\SmsContext\Entity\DeliveryReport;
use Ptx\Context\SmsContext\ValueObject\TimestampVO;
use Ptx\Context\SmsContext\UseCase\HandleIncomingSms\HandleIncomingSmsResponse;

class HandleIncomingSmsResponseTest extends \PHPUnit\Framework\TestCase
{
    const SMS_ID             = 2;
    const SMS_SENDER         = '+4917637704911';
    const SMS_CONTENT        = 'This is my first sms';
    const SMS_RECIPIENT      = 1982;
    const DELIVERY_REPORT_ID = 1;
    const DELIVERY_STATUS_ID = 2;
    const DELIVERY_TIMESTAMP = 1489212723;

    private $response;
    private $smsEntity;

    protected function setUp()
    {
        $smsEntity = new IncomingSms();
        $smsEntity
            ->setId(self::SMS_ID)
            ->setSender(self::SMS_SENDER)
            ->setRecipient(self::SMS_RECIPIENT)
            ->setContent(self::SMS_CONTENT);

        $deliveryReport = new DeliveryReport();
        $deliveryReport
            ->setId(self::DELIVERY_REPORT_ID)
            ->setSmsId(self::SMS_ID)
            ->setStatus(self::DELIVERY_STATUS_ID)
            ->setTimestamp(new TimestampVO(self::DELIVERY_TIMESTAMP));

        $this->response = new HandleIncomingSmsResponse($smsEntity, $deliveryReport);
    }

    protected function tearDown()
    {
        unset($this->response);
    }

    public function testGetRecipient()
    {
        $this->assertEquals(self::SMS_RECIPIENT, $this->response->getRecipient());
    }

    public function testGetContent()
    {
        $this->assertEquals(self::SMS_CONTENT, $this->response->getContent());
    }

    public function testGetSender()
    {
        $this->assertEquals(self::SMS_SENDER, $this->response->getSender());
    }

    public function testGetId()
    {
        $this->assertEquals(self::SMS_ID, $this->response->getId());
    }

    public function testGetDeliveryStatus()
    {
        $this->assertEquals(self::DELIVERY_STATUS_ID, $this->response->getDeliveryStatus());
    }
}
