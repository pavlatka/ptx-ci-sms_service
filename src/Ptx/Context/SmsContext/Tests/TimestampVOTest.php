<?php declare(strict_types=1);

namespace Ptx\Context\SmsContexts\Tests;

use Ptx\Context\SmsContext\ValueObject\TimestampVO;

class TimestampVOTest extends \PHPUnit\Framework\TestCase
{
    private $timestamp = 1489166437;
    private $valueObject;

    protected function setUp()
    {
        $this->valueObject = new TimestampVO($this->timestamp);
    }

    protected function tearDown()
    {
        unset($this->valueObject);
    }

    public function testGetTimestamp()
    {
        $result = $this->valueObject->getTimestamp();

        $this->assertEquals($this->timestamp, $result);
    }
}
