<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Tests;

use Ptx\Service\StringModifier;
use Ptx\Context\SmsContext\Entity\ShortCode;
use Ptx\Context\SmsContext\Factory\ShortCodeFactory;

class ShortCodeFactoryTest extends \PHPUnit\Framework\TestCase
{
    private $factory;
    private $stringModifier;

    protected function setUp()
    {
        $this->stringModifier = new StringModifier();
        $this->factory        = new ShortCodeFactory($this->stringModifier);
    }

    protected function tearDown()
    {
        unset($this->stringModifier, $this->factory);
    }

    public function testCreateEntityReturnsProperEntityAndMapping()
    {
        $data   = array();
        $entity = $this->factory->createEntity($data);

        $isValid = $entity instanceOf ShortCode;
        $this->assertTrue($isValid);

        $this->assertEquals(null, $entity->getId());
        $this->assertEquals(null, $entity->getConfirmationText());
    }

    public function testCreateEntityMapsDataProperly()
    {
        $data = array(
            'id'                => 1982,
            'confirmation_text' => 'Thank you for your message.'
        );
        $entity = $this->factory->createEntity($data);

        $this->assertEquals($data['id'], $entity->getId());
        $this->assertEquals($data['confirmation_text'], $entity->getConfirmationText());
    }
}
