<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Tests;

use Ptx\Service\StringModifier;
use Ptx\Context\SmsContext\Entity\DeliveryReport;
use Ptx\Context\SmsContext\ValueObject\TimestampVO;
use Ptx\Context\SmsContext\Factory\DeliveryReportFactory;

class DeliveryReportFactoryTest extends \PHPUnit\Framework\TestCase
{
    private $factory;
    private $stringModifier;

    protected function setUp()
    {
        $this->stringModifier = new StringModifier();
        $this->factory        = new DeliveryReportFactory($this->stringModifier);
    }

    protected function tearDown()
    {
        unset($this->stringModifier, $this->factory);
    }

    public function testCreateEntityReturnsProperEntityAndMapping()
    {
        $data   = array();
        $entity = $this->factory->createEntity($data);

        $isValid = $entity instanceOf DeliveryReport;
        $this->assertTrue($isValid);

        $this->assertEquals(null, $entity->getId());
        $this->assertEquals(null, $entity->getTimestamp());
        $this->assertEquals(null, $entity->getStatus());
        $this->assertEquals(null, $entity->getSmsId());
    }

    public function testCreateEntityMapsDataProperly()
    {
        $data = array(
            'id'        => 10,
            'timestamp' => 1489176275,
            'status'    => 1,
            'sms_id'    => 5
        );
        $entity = $this->factory->createEntity($data);

        $this->assertEquals($data['id'], $entity->getId());
        $this->assertEquals($data['status'], $entity->getStatus());
        $this->assertEquals($data['sms_id'], $entity->getSmsId());

        $timestamp = $entity->getTimestamp();
        $isValid   = $timestamp instanceOf TimestampVO;
        $this->assertTrue($isValid);
    }
}
