<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Tests;

use Ptx\Context\SmsContext\UseCase\HandleIncomingSms\HandleIncomingSmsRequest;

class HandleIncomingSmsRequestTest extends \PHPUnit\Framework\TestCase
{
    const SMS_SENDER    = '+4917637704911';
    const SMS_CONTENT   = 'This is my message';
    const SMS_RECIPIENT = 1982;

    private $request;

    protected function setUp()
    {
        $this->request = new HandleIncomingSmsRequest(
            self::SMS_SENDER,
            self::SMS_RECIPIENT,
            self::SMS_CONTENT
        );
    }

    protected function tearDown()
    {
        unset($this->request);
    }

    public function testGetRecipient()
    {
        $this->assertEquals(self::SMS_RECIPIENT, $this->request->getRecipient());
    }

    public function testGetContent()
    {
        $this->assertEquals(self::SMS_CONTENT, $this->request->getContent());
    }

    public function testGetSender()
    {
        $this->assertEquals(self::SMS_SENDER, $this->request->getSender());
    }
}
