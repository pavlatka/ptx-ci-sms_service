<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Tests;

use Ptx\Context\SmsContext\Entity\ShortCode;

class ShortCodeTest extends \PHPUnit\Framework\TestCase
{
    private $entity;

    protected function setUp()
    {
        $this->entity = new ShortCode();
    }

    protected function tearDown()
    {
        unset($this->entity);
    }

    public function testSetGetId()
    {
        $value = 1987;
        $this->entity->setId($value);

        $this->assertEquals($value, $this->entity->getId());
    }

    public function testSetGetConfirmationText()
    {
        $value = 'Thank you for your message.';
        $this->entity->setConfirmationText($value);

        $this->assertEquals($value, $this->entity->getConfirmationText());
    }
}
