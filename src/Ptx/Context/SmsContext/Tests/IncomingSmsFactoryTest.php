<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Tests;

use Ptx\Service\StringModifier;
use Ptx\Context\SmsContext\Entity\IncomingSms;
use Ptx\Context\SmsContext\Factory\IncomingSmsFactory;

class IncomingSmsFactoryTest extends \PHPUnit\Framework\TestCase
{
    private $factory;
    private $stringModifier;

    protected function setUp()
    {
        $this->stringModifier = new StringModifier();
        $this->factory        = new IncomingSmsFactory($this->stringModifier);
    }

    protected function tearDown()
    {
        unset($this->stringModifier, $this->factory);
    }

    public function testCreateEntityReturnsProperEntityAndMapping()
    {
        $data   = array();
        $entity = $this->factory->createEntity($data);

        $isValid = $entity instanceOf IncomingSms;
        $this->assertTrue($isValid);

        $this->assertEquals(null, $entity->getId());
        $this->assertEquals(null, $entity->getRecipient());
        $this->assertEquals(null, $entity->getSender());
        $this->assertEquals(null, $entity->getContent());
    }

    public function testCreateEntityMapsDataProperly()
    {
        $data = array(
            'id'        => 10,
            'recipient' => 1987,
            'sender'    => '+4917637704911',
            'content'   => 'This is my private sms.'
        );
        $entity = $this->factory->createEntity($data);

        $this->assertEquals($data['id'], $entity->getId());
        $this->assertEquals($data['recipient'], $entity->getRecipient());
        $this->assertEquals($data['sender'], $entity->getSender());
        $this->assertEquals($data['content'], $entity->getContent());
    }
}
