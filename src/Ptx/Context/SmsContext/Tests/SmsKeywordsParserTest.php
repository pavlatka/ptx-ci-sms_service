<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Service;

class SmsKeywordsParserTest extends \PHPUnit\Framework\TestCase
{
    private $service;

    protected function setUp()
    {
        $this->service = new SmsKeywordsParser();
    }

    protected function tearDown()
    {
        unset($this->service);
    }

    public function dataGenerateData()
    {
        return array(
            array('LIM this is my story', 'LIM'),
            array(' LIM this is my story', 'LIM'),
            array('LAR this is my story', 'LAR')
        );
    }

    /**
     * @dataProvider dataGenerateData
     */
    public function testParseKeywordFromStringReturnsCorrectKeywords(string $string, string $expected)
    {
        $result = $this->service->parseKeywordFromString($string);

        $this->assertEquals($expected, $result);
    }
}
