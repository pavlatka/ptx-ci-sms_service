<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Tests;

use Ptx\Context\SmsContext\Entity\Customer;

class CustomerTest extends \PHPUnit\Framework\TestCase
{
    private $entity;

    protected function setUp()
    {
        $this->entity = new Customer();
    }

    protected function tearDown()
    {
        unset($this->entity);
    }

    public function testSetGetId()
    {
        $value = 10;
        $this->entity->setId($value);

        $this->assertEquals($value, $this->entity->getId());
    }

    public function testSetGetSmsCode()
    {
        $value = 10;
        $this->entity->setSmsCode($value);

        $this->assertEquals($value, $this->entity->getSmsCode());
    }
}
