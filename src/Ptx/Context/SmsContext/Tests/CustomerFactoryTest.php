<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Tests;

use Ptx\Service\StringModifier;
use Ptx\Context\SmsContext\Entity\Customer;
use Ptx\Context\SmsContext\Factory\CustomerFactory;

class CustomerFactoryTest extends \PHPUnit\Framework\TestCase
{
    private $factory;
    private $stringModifier;

    protected function setUp()
    {
        $this->stringModifier = new StringModifier();
        $this->factory        = new CustomerFactory($this->stringModifier);
    }

    protected function tearDown()
    {
        unset($this->stringModifier, $this->factory);
    }

    public function testCreateEntityReturnsProperEntityAndMapping()
    {
        $data   = array();
        $entity = $this->factory->createEntity($data);

        $isValid = $entity instanceOf Customer;
        $this->assertTrue($isValid);

        $this->assertEquals(null, $entity->getId());
        $this->assertEquals(null, $entity->getSmsCode());
    }

    public function testCreateEntityMapsDataProperly()
    {
        $data = array(
            'id'       => 10,
            'sms_code' => 1982
        );
        $entity = $this->factory->createEntity($data);

        $this->assertEquals($data['id'], $entity->getId());
        $this->assertEquals($data['sms_code'], $entity->getSmsCode());
    }
}
