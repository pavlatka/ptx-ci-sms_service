<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Tests;

use Ptx\Context\SmsContext\Entity\DeliveryReport;
use Ptx\Context\SmsContext\ValueObject\TimestampVO;

class DeliveryReportTest extends \PHPUnit\Framework\TestCase
{
    private $entity;
    private $timestampVO;

    protected function setUp()
    {
        $this->entity      = new DeliveryReport();
        $this->timestampVO = $this->getMockBuilder('\Ptx\Context\SmsContext\ValueObject\TimestampVO')
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        unset($this->entity, $this->timestampVO);
    }

    public function testSetGetId()
    {
        $value = 10;
        $this->entity->setId($value);

        $this->assertEquals($value, $this->entity->getId());
    }

    public function testSetGetTimestamp()
    {
        $this->entity->setTimestamp($this->timestampVO);

        $result = $this->entity->getTimestamp();
        $isValid = $result instanceOf TimestampVO;

        $this->assertTrue($isValid);
    }

    public function testSetGetStatus()
    {
        $value = 10;
        $this->entity->setStatus($value);

        $this->assertEquals($value, $this->entity->getStatus());
    }

    public function testSetGetSmsId()
    {
        $value = 10;
        $this->entity->setSmsId($value);

        $this->assertEquals($value, $this->entity->getSmsId());
    }
}
