<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Tests;

use Ptx\Context\SmsContext\Entity\IncomingSms;

class IncomingSmsTest extends \PHPUnit\Framework\TestCase
{
    private $entity;

    protected function setUp()
    {
        $this->entity = new IncomingSms();
    }

    protected function tearDown()
    {
        unset($this->entity);
    }

    public function testSetGetId()
    {
        $value = 10;
        $this->entity->setId($value);

        $this->assertEquals($value, $this->entity->getId());
    }

    public function testSetGetSender()
    {
        $value = '+4917637704911';
        $this->entity->setSender($value);

        $this->assertEquals($value, $this->entity->getSender());
    }

    public function testSetGetRecipient()
    {
        $value = 1010;
        $this->entity->setRecipient($value);

        $this->assertEquals($value, $this->entity->getRecipient());
    }

    public function testSetGetContent()
    {
        $value = 'content';
        $this->entity->setContent($value);

        $this->assertEquals($value, $this->entity->getContent());
    }
}
