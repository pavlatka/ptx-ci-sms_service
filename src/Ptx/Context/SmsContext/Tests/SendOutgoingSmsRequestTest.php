<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Tests;

use Ptx\Context\SmsContext\UseCase\SendOutgoingSms\SendOutgoingSmsRequest;

class SendOutgoingSmsRequestTest extends \PHPUnit\Framework\TestCase
{
    const SMS_SENDER    = 1982;
    const SMS_RECIPIENT = '+4917637704911';

    private $request;

    protected function setUp()
    {
        $this->request = new SendOutgoingSmsRequest(
            self::SMS_SENDER,
            self::SMS_RECIPIENT
        );
    }

    protected function tearDown()
    {
        unset($this->request);
    }

    public function testGetRecipient()
    {
        $this->assertEquals(self::SMS_RECIPIENT, $this->request->getRecipient());
    }

    public function testGetSender()
    {
        $this->assertEquals(self::SMS_SENDER, $this->request->getSender());
    }
}
