<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Service;

class SmsKeywordsParser
{
    public function parseKeywordFromString(string $string) : string
    {
        return explode(' ', trim($string))[0];
    }
}
