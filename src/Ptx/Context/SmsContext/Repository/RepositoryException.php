<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Repository;

class RepositoryException extends \Exception
{
    const ERROR_NOT_FOUND     = 404;
    const ERROR_RUNTIME_ERROR = 500;
}
