<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Repository;

use Ptx\Context\SmsContext\Entity\ShortCode;
use Ptx\Context\SmsContext\Entity\IncomingSms;
use Ptx\Context\SmsContext\Factory\IncomingSmsFactory;

class IncomingSmsRepository extends BaseRepository implements \ArrayAccess
{
    private $dao;
    private $factory;

    public function __construct(
        IncomingSmsDao $dao,
        IncomingSmsFactory $factory
    ) {
        $this->dao     = $dao;
        $this->factory = $factory;
    }

    public function createSms(
        ShortCode $shortCode,
        string $sender,
        string $content
    ) : IncomingSms {
        $recipient = $shortCode->getId();

        return $this->factory->createEntity(compact('recipient', 'sender', 'content'));
    }

    public function persists(IncomingSms $incomingSms)
    {
        $this->dao->persists($incomingSms);
    }
}
