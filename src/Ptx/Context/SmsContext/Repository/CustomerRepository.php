<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Repository;

use Ptx\Context\SmsContext\Entity\Customer;
use Ptx\Context\SmsContext\Factory\CustomerFactory;

class CustomerRepository extends BaseRepository implements \ArrayAccess
{
    private $dao;
    private $factory;

    public function __construct(
        CustomerDao $dao,
        CustomerFactory $factory
    ) {
        $this->dao     = $dao;
        $this->factory = $factory;
    }

    public function persists(Customer $customer)
    {
        $this->dao->persists($customer);
    }
}
