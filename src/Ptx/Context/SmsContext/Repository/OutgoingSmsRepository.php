<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Repository;

use Ptx\Context\SmsContext\Entity\OutgoingSms;
use Ptx\Context\SmsContext\Factory\OutgoingSmsFactory;

class OutgoingSmsRepository extends BaseRepository implements \ArrayAccess
{
    private $dao;
    private $factory;

    public function __construct(
        OutgoingSmsDao $dao,
        OutgoingSmsFactory $factory
    ) {
        $this->dao     = $dao;
        $this->factory = $factory;
    }

    public function createSms(
        string $recipient,
        int $sender,
        string $content
    ) : OutgoingSms {
        return $this->factory->createEntity(compact('recipient', 'sender', 'content'));
    }

    public function persist(OutgoingSms $outgoingSms)
    {
        $this->dao->persist($outgoingSms);
    }
}
