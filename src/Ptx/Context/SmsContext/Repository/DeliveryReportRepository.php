<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Repository;

use Ptx\Context\SmsContext\Entity\DeliveryReport;
use Ptx\Context\SmsContext\Factory\DeliveryReportFactory;

class DeliveryReportRepository extends BaseRepository implements \ArrayAccess
{
    private $dao;
    private $factory;

    public function __construct(
        DeliveryReportDao $dao,
        DeliveryReportFactory $factory
    ) {
        $this->dao     = $dao;
        $this->factory = $factory;
    }

    public function createDeliveryReport4Sms(BaseSms $sms, int $status) : DeliveryReport
    {
        $data = array(
            'sms_id'    => $sms->getId(),
            'status'    => $status,
            'timestamp' => time()
        );

        return $this->factory->createEntity($data);
    }

    public function persists(DeliveryReport $deliveryReport)
    {
        $this->dao->persists($deliveryReport);
    }
}
