<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Repository;

abstract class BaseRepository implements \ArrayAccess
{
    protected $container = array();

    public function offsetSet($offset, $value) : void
    {
        $this->container[$offset] = $value;
    }

    public function offsetExists($offset) : bool
    {
        return isset($this->container[$offset]);
    }

    public function offsetUnset($offset) : void
    {
        unset($this->container[$offset]);
    }

    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }
}
