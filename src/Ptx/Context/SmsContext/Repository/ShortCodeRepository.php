<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Repository;

use Ptx\Context\SmsContext\Entity\ShortCode;
use Ptx\Context\SmsContext\Factory\ShortCodeFactory;

class ShortCodeRepository extends BaseRepository implements \ArrayAccess
{
    private $dao;
    private $factory;

    public function __construct(
        ShortCodeDao $dao,
        ShortCodeFactory $factory
    ) {
        $this->dao     = $dao;
        $this->factory = $factory;
    }

    public function getShortCode4Code(int $code) : ShortCode
    {
        if (isset($this->container[$code])) {
            return $this->container[$code];
        }

        $data = $this->dao->getShortCode4Code($code);
        if (count($data) === 0) {
            throw new \RepositoryException(
                'There is not short code for code specified [C:' . $code . ']',
                RepositoryException::ERROR_NOT_FOUND
            );
        }

        $shortCode = $this->factory->createEntity($data);

        $this->container[$code] = $shortCode;

        return $shortCode;
    }

    public function persists(ShortCode $customer)
    {
        $this->dao->persists($customer);
    }
}
