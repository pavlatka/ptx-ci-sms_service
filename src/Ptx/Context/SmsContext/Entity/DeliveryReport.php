<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Entity;

use Ptx\Context\SmsContext\ValueObject\TimestampVO;

class DeliveryReport extends BaseEntity
{
    const STATUS_ACCEPTED   = 1; // Accepted by system
    const STATUS_PROCESSING = 2; // Sending via Gateway
    const STATUS_SEND       = 3; // Sent by Gateway
    const STATUS_RECEIVED   = 4; // Received by Recipient

    private $smsId;
    private $status;
    private $timestamp;

    public function setSmsId(int $smsId) : DeliveryReport
    {
        $this->smsId = $smsId;

        return $this;
    }

    public function getSmsId() : ?int
    {
        return $this->smsId;
    }

    public function setTimestamp(TimestampVO $timestamp) : DeliveryReport
    {
        $this->timestamp = clone $timestamp;

        return $this;
    }

    public function getTimestamp() : ?TimestampVO
    {
        return $this->timestamp;
    }

    public function setStatus(int $status) : DeliveryReport
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus() : ?int
    {
        return $this->status;
    }
}
