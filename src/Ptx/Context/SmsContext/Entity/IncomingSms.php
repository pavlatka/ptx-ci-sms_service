<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Entity;

class IncomingSms extends BaseSms
{
    private $sender;
    private $recipient;

    public function setRecipient(int $recipient) : IncomingSms
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function getRecipient() : ?int
    {
        return $this->recipient;
    }

    public function setSender(string $sender) : IncomingSms
    {
        $this->sender = $sender;

        return $this;
    }

    public function getSender() : ?string
    {
        return $this->sender;
    }
}
