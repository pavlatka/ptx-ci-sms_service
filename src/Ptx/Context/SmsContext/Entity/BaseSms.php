<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Entity;

abstract class BaseSms extends BaseEntity
{
    protected $content;

    public function setContent(string $content) : BaseSms
    {
        $this->content = $content;

        return $this;
    }

    public function getContent() : ?string
    {
        return $this->content;
    }
}
