<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Entity;

use Ptx\Context\SmsContext\ValueObject\TimestampVO;

class Customer extends BaseEntity
{
    private $smsCode;

    public function setSmsCode(int $smsCode) : Customer
    {
        $this->smsCode = $smsCode;

        return $this;
    }

    public function getSmsCode() : ?int
    {
        return $this->smsCode;
    }

}
