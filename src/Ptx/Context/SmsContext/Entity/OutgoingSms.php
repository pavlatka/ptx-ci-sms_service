<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Entity;

class OutgoingSms extends BaseSms
{
    private $recipient;
    private $sender;

    public function setRecipient(string $recipient) : OutgoingSms
    {
        $this->recipient = $recipient;

        return $this;
    }

    public function getRecipient() : ?string
    {
        return $this->recipient;
    }

    public function setSender(int $sender) : OutgoingSms
    {
        $this->sender = $sender;

        return $this;
    }

    public function getSender() : ?int
    {
        return $this->sender;
    }
}
