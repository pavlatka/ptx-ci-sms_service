<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Entity;

abstract class BaseEntity
{
    protected $id;

    public function setId(int $id) : BaseEntity
    {
        $this->id = $id;

        return $this;
    }

    public function getId() : ?int
    {
        return $this->id;
    }
}
