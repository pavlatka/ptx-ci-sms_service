<?php declare(strict_types=1);

namespace Ptx\Context\SmsContext\Entity;

use Ptx\Context\SmsContext\ValueObject\TimestampVO;

class ShortCode extends BaseEntity
{
    private $confirmationText;

    public function setConfirmationText($confirmationText)
    {
        $this->confirmationText = $confirmationText;

        return $this;
    }

    public function getConfirmationText()
    {
        return $this->confirmationText;
    }
}
