<?php
declare(strict_types=1);

namespace Ptx\Service;

class StringModifier
{
    public function camelize(string $string, string $separator = '_') : string
    {
        return str_replace($separator, '', ucwords($string, $separator));
    }
}
