<?php declare(strict_types=1);

namespace Ptx\Tests;

use Ptx\Service\StringModifier;

class StringModifierTest extends \PHPUnit\Framework\TestCase
{
    private $service;

    protected function setUp()
    {
        $this->service = new StringModifier();
    }

    protected function tearDown()
    {
        unset($this->service);
    }

    public function dataTestCamilizeReturnCorrectString()
    {
        return array(
            array('test_string', '_', 'TestString'),
            array('another_test_string', '_', 'AnotherTestString'),
            array('try-with-different-separator', '-', 'TryWithDifferentSeparator'),
        );
    }

    /**
     * @dataProvider dataTestCamilizeReturnCorrectString
     */
    public function testCamilizeReturnCorrectString(string $string, string $separator, string $expected)
    {
        $result = $this->service->camelize($string, $separator);

        $this->assertEquals($expected, $result);
    }
}
