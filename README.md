# SMS Service #

Create classes to model a simple SMS Service and its components. This will be a
service that receives SMSes from users, processes them, sends back a reply, and then receives
the delivery report for that reply.

##2 types of SMS##
* Outgoing (from the Service to the User),
* Incoming (from the User to the Service).

These would be like the normal SMSes we send and receive with our phones between friends, only the
receipient for Incoming SMSes (and sender for Outgoing SMSes) would be a 4-digit "shortcode".

Also model the delivery report for an Outgoing SMS, call it DLR.

## What fields do you need to define in each one? ##

All 3 SMS classes (Incoming, Outgoing, DLR) must have a saveToDB() method (just add the method signature)

Outgoing must have a send() method that will send the SMS to the user (just add the method signature)

## Model an SMS Service: ##

Service must have a handleIncomingSMS() function that expects an Incoming (just add the method signature)
Service must have a handleDLR() function that expects a DLR (just add the method signature)

Try to reuse code as much as possible.

Now, create a class to model the service for LimassolFM Radio Station that expects SMSes from users,
and replies back to them with a simple SMS.
The class should implement the function handleIncomingSMS() to receive the SMS, save it into the DB, and
and send as a response a new Outgoing object with body = "Thank you for your message".

Finally, do another service to handle SMSes from a different radio station, namely one called LarnacaFM.
How would you handle this efficiently, given that you already have one for LimassolFM?
